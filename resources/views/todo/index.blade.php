@extends('todo.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>TODO</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('todo.create') }}"> Create New</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($todos as $todo)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $todo->name }}</td>
                <td>
                    <form action="{{ route('todo.destroy',$todo->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('todo.show',$todo->id) }}">Show</a>
                        <a class="btn btn-primary" href="{{ route('todo.edit',$todo->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

@endsection
