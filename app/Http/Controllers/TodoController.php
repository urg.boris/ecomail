<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index()
    {
        $todos = Todo::all();
        return view('todo.index', compact('todos'))
            ->with('i', (request()->input('page', 1)));
    }

    public function create()
    {
        return view('todo.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            ['name' => 'required']
        );
        Todo::factory()->create(
            [
                'name' => $request['name'],
            ]
        );

        return redirect()->route('todo.index')->with('success', 'Todo created.');
    }

    public function show(Todo $todo)
    {
        return view('todo.show', compact('todo'));
    }

    public function edit(Todo $todo)
    {
        return view('todo.edit', compact('todo'));
    }


    public function update(Request $request, Todo $todo)
    {
        $request->validate(
            ['name' => 'required']
        );
        $todo->update($request->all());

        return redirect()->route('todo.index')->with('success', 'Todo created.');
    }


    public function destroy(Todo $todo)
    {
        $todo->delete();
        return redirect()->route('todo.index')->with('success', 'Todo deleted.');
    }
}
